import React, { useState } from "react";
import { useSelector } from "react-redux";
import { BoxIcon, DropMenu, Hamburguer, DropDiv, DropLink } from "./style";

const DropDownMenu = () => {
  const session = useSelector((state) => state.session);
  const [menuActive, setMenuActive] = useState(true);

  return (
    <DropMenu
      menuActive={menuActive.toString()}
      onMouseLeave={() => setMenuActive(false)}
      onMouseEnter={() => setMenuActive(true)}
    >
      {/* <BoxIcon onClick={() => setMenuActive(menuActive)}>
        <Hamburguer menuactive={menuActive.toString()}></Hamburguer>
      </BoxIcon>
      <DropDiv menuactive={menuActive.toString()}>
        <DropLink menuactive={menuActive.toString()} to="/explorar">
          Explorar
        </DropLink>
        <DropLink menuactive={menuActive.toString()} to="/pesquisa">
          Buscar
        </DropLink>   
        <DropLink
          menuactive={menuActive.toString()}
          to={`/perfil/${session.user.id}`}
        >
          Perfil
        </DropLink>
      </DropDiv> */}
    </DropMenu>
  );
};

export default DropDownMenu;
