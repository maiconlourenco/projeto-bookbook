import React from "react";
import { motion } from "framer-motion";
import styled from "styled-components";

const PageTransition = ({ children }) => {
  return (
    <PageTransitionContainer
      initial="pageInitial"
      animate="pageAnimate"
      variants={{
        pageInitial: {
          opacity: 0,
        },
        pageAnimate: {
          opacity: 1,
          transition: {
            delay: 0.3,
          },
        },
      }}
    >
      {children}
    </PageTransitionContainer>
  );
};

export default PageTransition;

const PageTransitionContainer = styled(motion.div)`
  position: relative;
  overflow: hidden;
`;
