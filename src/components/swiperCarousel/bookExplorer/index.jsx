import React from "react";
import { useSelector, useDispatch } from "react-redux";
import UserIcon from "../../../assets/icons/profile-icon.png";
import BookBlank from "../../../assets/img/book-blank.png";
import { updateFriends } from "../../../redux/actions/friends";
import {
  Book,
  BookImg,
  FallBackContent,
  InnerImage,
  Content,
  Title,
  Rating,
  User,
  UserImage,
  UserReview,
  UserName,
  BtnContainer,
  AddButton,
} from "./styled-book";

const CardExplorer = ({ book }) => {
  const dispatch = useDispatch();
  const { user } = useSelector((state) => state.session);

  return (
    <>
      <Book>
        <BookImg
          src={book.image_url ? book.image_url : BookBlank}
          alt="book-image"
        />
      </Book>
      <>
        <FallBackContent>
          <InnerImage
            src={book.image_url ? book.image_url : BookBlank}
            alt="book"
          />
          <Content>
            <Title>{book.title}</Title>
            <Rating disabled defaultValue={book.grade} />
            <User to={`/perfil/${book.creator.id}`}>
              <UserImage
                src={book.creator.image_url ? book.creator.image_url : UserIcon}
              />
              <UserReview>
                "
                {book.review
                  ? book.review
                  : "Este usuário não deixou nenhuma avaliação para este livro."}
                "
              </UserReview>
            </User>
            <UserName to={`/perfil/${book.creator.id}`}>
              <p>Adicionado por </p>
              <h2>{book.creator.user}</h2>
            </UserName>
            <BtnContainer>
              {book.creator.id !== user.id && (
                <AddButton
                  onClick={() => {
                    dispatch(updateFriends(user, book.creator));
                  }}
                >
                  Adicionar amigo
                </AddButton>
              )}
            </BtnContainer>
          </Content>
        </FallBackContent>
      </>
    </>
  );
};

export default CardExplorer;
